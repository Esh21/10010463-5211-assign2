﻿using System;
using System.Diagnostics;
namespace question05
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Print out the division table for:");
            

            var number = Convert.ToDouble(Console.ReadLine());
            string c = string.Format("{0:00}", number);

            for(var i = 0; i < 12; i++) {
                var a = i + 1;
                var v = a / number;
                 var round = Math.Round(v, 2);
                string b = string.Format("{0:00}", a);
                
                if (a <= 10 | v <= 10 | number <= 10)
                {

                    
                    Console.WriteLine($"{b} / {c} = 0{round}");
                  
                }
                else
                {

                    Console.WriteLine($"{a} /{number} = {round}");
                }
                
               
            }
            Console.Read();
        } 
    }
}
